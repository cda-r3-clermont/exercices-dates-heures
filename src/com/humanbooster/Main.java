package com.humanbooster;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); //Objet scanner

        System.out.println("Entre ta date de naissance");//saisiie utilisateur premiere date
        String dateNaissance = scanner.nextLine();
        System.out.println("Entre la date de naissance de ton conjoint");//saisie deuxieme date
        String dateNaissanceConjoint = scanner.nextLine();

        GregorianCalendar calendar = new GregorianCalendar();
        String[] dNaissanceArray = dateNaissance.split("/");

        // Attention, les mois commencent à 0 donc je fais un -1
        GregorianCalendar date1 = new GregorianCalendar(Integer.parseInt(dNaissanceArray[2]), Integer.parseInt(dNaissanceArray[1]) - 1, Integer.parseInt(dNaissanceArray[0]));
        // Il faut réccupérer les jours après avoir fait le calendrier
        int jour1 = date1.get(Calendar.DAY_OF_MONTH);
        int mois1 = date1.get(Calendar.MONTH);
        int annee1 = date1.get(Calendar.YEAR);

        int jour2 = calendar.get(Calendar.DAY_OF_MONTH);
        int mois2 = calendar.get(Calendar.MONTH);
        int annee2 = calendar.get(Calendar.YEAR);

        GregorianCalendar date2 = new GregorianCalendar(annee2, mois2, jour2);

        // Calculer le nombre de jours entre les deux dates
        long difference = (date2.getTimeInMillis() - date1.getTimeInMillis()) / (24 * 60 * 60 * 1000);

    }
}
