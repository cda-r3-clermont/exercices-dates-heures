package com.humanbooster;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Main2 {

    public static void main(String[] args) {
        // Je demande à l'utilisateur une saisie
        System.out.println("Veuillez saisir votre date de naissance (dd/mm/yyyy)");

        // Je déclare un scanner qui s'occupera de réccupérer les saisies utilisateur
        Scanner scanner = new Scanner(System.in);
        // Je réccupére la première saisie
        String date1Str = scanner.nextLine();
        // Je transforme la première saisie en tableau avec le séparateur "/"
        // La méthode split transforme une chaine de caractères en tableau
        // J'aurais un tableau avec 3 entrées [ 0=> "jour", 1=>"mois", 2=>"annee"]
        String[] date1StrArray = date1Str.split("/");

        // Je réccupére et je transforme en integer la valeur saisie dans le jour
        int dayOne = Integer.parseInt(date1StrArray[0]);
        // Je réccupére et je transforme en integer la valeur saisie dans le mois
        int monthOne = Integer.parseInt(date1StrArray[1]);
        // Je réccupére et je transforme en integer la valeur saisie dans l'année
        int yearOne = Integer.parseInt(date1StrArray[2]);

        // Je fais exactement la même sur la deuxième date saisie
        System.out.println("Veuillez saisir la date de naissance  de votre conjoint(dd/mm/yyyy)");
        String date2Str = scanner.nextLine();
        String[] date2StrArray = date2Str.split("/");

        int dayTwo = Integer.parseInt(date2StrArray[0]);
        int monthTwo = Integer.parseInt(date2StrArray[1]);
        int yearTwo = Integer.parseInt(date2StrArray[2]);

        // Pour chacune de mes saisies, je cré mon objet GregorianCalendar
        // Ceci nous permettra l'utilisation des méthodes proposées par la classe
        // (before, after, getTimeInMillis, add, get)
        GregorianCalendar calendarOne = new GregorianCalendar(yearOne, monthOne-1, dayOne);
        GregorianCalendar calendarTwo = new GregorianCalendar(yearTwo, monthTwo-1, dayTwo);

        // Je réccupére le timestamp de mes dates
        // Il nous servira ensuite pour calculer la différence
        // entre les deux dates en jour
        // Je fais cela avant le if suivant. Je pourrais ainsi adapter et avoir systématiquement
        // une valeur positive en fonction des conditions suivantes
        Long calendarOneTimestamp = calendarOne.getTimeInMillis() / 1000;
        Long calendarTwoTimeStamp = calendarTwo.getTimeInMillis() / 1000;

        // Je déclare une variable difference que j'utiliserais ensuite
        Long difference;
        // Si ma date calendarOne est avant calendarTwo
        // alors, je suis plus vieux que mon conjoint
        if(calendarOne.before(calendarTwo)){
            System.out.println("Vous êtes plus vieux que votre conjoint");
            // Si je suis plus vieux que mon conjoint,
            // Son timestamp - Mon timestamp me donnera notre écart avec une valeur positive
            difference = calendarTwoTimeStamp - calendarOneTimestamp;

            // Sinon, je suis peut être plus jeune ??
        } else if (calendarOne.after(calendarTwo)){
            System.out.println("Vous êtes plus jeune que votre conjoint");
            // Si je suis plus jeune que mon conjoint
            // alors son timestamp moins le mien sera positif
            difference = calendarOneTimestamp - calendarTwoTimeStamp ;
        } else {
            System.out.println("Peut être vous êtes vous croisé à la maternité ?");
            difference = Long.valueOf(0);
        }

        // J'utilise la variable différence pour calculer l'écart en jour
        int nbDays = (int)(difference / (60*60*24));
        // J'affiche le resultat
        System.out.println("Vous avez " + nbDays + "jours d'écart");

        // La méthode add de la classe GregorianCalendar nous permet
        // d'ajouter une durée à une date.
        // Ici pour chacune de mes dates, j'ajoute 67 ans
        calendarOne.add(GregorianCalendar.YEAR, 67);
        calendarTwo.add(GregorianCalendar.YEAR, 67);

        // J'affiche ensuite la valeur modifiée pour afficher la date de la retraite
        System.out.println("Vous serez à la retraite le "
                + calendarOne.get(Calendar.YEAR) + '/'
                + calendarOne.get(Calendar.MONTH)+"/"
                + calendarOne.get(Calendar.DAY_OF_MONTH));

        System.out.println("Votre conjoint sera à la retraite le "
                + calendarTwo.get(Calendar.YEAR) + '/'
                + calendarTwo.get(Calendar.MONTH)+"/"
                + calendarTwo.get(Calendar.DAY_OF_MONTH));

    }
}
